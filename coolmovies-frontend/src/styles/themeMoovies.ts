import { createTheme } from "@mui/material";

const themeMoovies = createTheme({
    palette: {
      primary: {
        main: "#FF6B00",
        contrastText: "#fff",
      },
      secondary: {
        main: "#7879F1",
        contrastText: "#fff",
      },
  
      text: {
        primary: "#FCFCFC",
        secondary: "#ffffff85"
      },
      background: {
        default: "#454545",
        paper: "#0D0D0D",
      }
  
    },
  });

export default themeMoovies