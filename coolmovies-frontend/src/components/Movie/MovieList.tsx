import { css, keyframes } from '@emotion/react';
import { Typography } from '@mui/material';
import { useEffect } from 'react';
import { moviesActions, useAppDispatch, useAppSelector } from '../../redux';
import MovieCardItem from './MovieCardItem';
import ScrollReveal from '../ScrollReveal';


const typewriter = keyframes({
    from: {
        width: 0
    },
    to: {
        width: '100%'
    }
})

const styles = {
    container: css({
        margin: '32px 100px',
    }),
    title: css({
        fontSize: '1.6rem',
        fontWeight: 'bold',
        marginLeft: 15,

    }),
    hr: css({
        height: '1px',
        border: 'none',
        borderBottom: '1px solid #FF6B00',
        animation: `${typewriter} 1s steps(44) 1s 1 normal both, blinkTextCursor 500ms steps(44) infinite normal`,

    }),
    list: css({
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        rowGap: 35,
        columnGap: 30,
        padding: 16,
    }),
};

const MovieList: React.FC = () => {
    const dispatch = useAppDispatch();
    const moviesState = useAppSelector((state) => state.movies);

    useEffect(() => {
        dispatch(moviesActions.fetch());
    }, [dispatch]);


    return (
        <div css={styles.container}>
            <Typography css={styles.title} variant="h2" component="h2">
                Tranding movies
                <hr css={styles.hr} />
            </Typography>

            <div css={styles.list}>
                {moviesState.data?.map((movie) => (
                    <ScrollReveal key={movie.id}>
                        <MovieCardItem key={movie.id} movie={movie} />
                    </ScrollReveal>
                ))}
            </div>

        </div>
    );
};

export default MovieList;
