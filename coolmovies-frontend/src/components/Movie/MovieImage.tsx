import React from 'react';
import { css } from '@emotion/react';


const styles = {
  root: css({
    display: 'flex',
    flexDirection: 'column',
    height: '400px',
    borderRadius: '10px',
    rowGap: '5px',
    marginBottom: '3rem'
  }),
  image: css({
    height: '400px',
    borderRadius: '5px',
  }),
};

const MovieImage: React.FC<{ imgUrl?: string }> = ({ imgUrl }) => {
  return (
    <div css={styles.root}>
      {imgUrl && (
        <img src={imgUrl} css={styles.image} alt={'Movie image'} />
      )}
    </div>
  );
};

export default MovieImage;
