import { Box, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import { Movie } from '../../interfaces/Movie';
import { useCallback } from 'react';
import { css } from '@emotion/react';


interface MovieCardProps {
  movie?: Movie;
}

const styles = {
  card: css({
    display: 'flex',
    flexDirection: 'column',
    width: 236,
    height: 300,
    justifySelf: 'center',
    borderRadius: '15px',
    cursor: 'pointer',
    '&:hover':{ opacity: '0.75' },
  }),
  image: css({
    height: '100%',
    width: '100%',
    borderRadius: '15px',
  }),
  content: css({
    display: 'flex',
    alignItems: 'flex-end',
    color: 'white',
    backgroundColor: 'transparent',
    height: 300,
    position: 'absolute',
    width: 236,
  }),
  titleContainer: css({
    background: 'linear-gradient(0deg, rgb(20, 20, 20) 0%, transparent 100%)',
    width: '100%',
    height: '35%',
    display: 'flex',
    alignItems: 'flex-end',
    paddingLeft: '10px',
    borderRadius: '0 0 15px 15px',
  }),
  title: css({
    fontSize: '1rem',
    paddingBottom: '10px',
  }),
};

const MovieCardItem: React.FC<MovieCardProps> = ({ movie }) => {
  const router = useRouter();
  const handleClick = useCallback(
    () => router.push(`/reviews/${movie?.id}`),
    [router, movie?.id]
  );
  return (
    <Box boxShadow={2} css={styles.card} onClick={handleClick}>
      <img css={styles.image} src={movie?.imgUrl} />
      <div css={styles.content}>
        <div css={styles.titleContainer}>
          <Typography css={styles.title}  gutterBottom variant="h6" component="div">
            {movie?.title}
          </Typography>
        </div>
      </div>
    </Box>
  );
};

export default MovieCardItem;
