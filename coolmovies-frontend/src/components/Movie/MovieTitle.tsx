import React from 'react';
import { Box, Chip, Tooltip, Typography } from '@mui/material';
import { Star } from '@mui/icons-material';
import { Movie } from '../../interfaces/Movie';
import { formatDate } from '../../util/date'
import { css } from '@emotion/react';
import { Review } from '../../interfaces/Review';

const styles = {
  root: css({
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '16px',
  }),
  boxInfo: css({
    display: 'flex',
    columnGap: '15px',
    alignItems: 'center',
    marginTop: '10px'
  }),
  chip: css({
    backgroundColor: '#fff',
    color: '#000',
  })
};

const MovieTitle: React.FC<{ movie?: Movie, reviews: Review[] }> = ({ movie, reviews }) => {
  function averageRatingReview() {
    if (reviews?.length) {
      const sum = reviews.reduce((acc, cur) => acc +  (cur ? cur.rating as number : 0), 0);
      return (sum / reviews.length).toFixed(1);
    }
    return 0;
  }
  return (
    <div css={styles.root}>
      <Typography variant={'h4'}>{movie?.title}</Typography>
      <Typography>Director: {movie?.director?.name}</Typography>
      <Box css={styles.boxInfo}>
        <Tooltip title={`Average rating: ${averageRatingReview()}`}>
          <Chip
            label={averageRatingReview()}
            icon={<Star sx={{ color: '#faaf00 !important' }} />}
            css={styles.chip} />
        </Tooltip>
        <Typography>
          {formatDate(movie?.releaseDate as string, 'DD MMM YYYY')}
        </Typography>
      </Box>
    </div>
  );
};

export default MovieTitle;
