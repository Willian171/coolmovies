import React, { useCallback } from 'react';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Rating,
  Stack,
} from '@mui/material';
import { Review } from '../../interfaces/Review';
import { Delete } from '@mui/icons-material';
import { moviesActions, useAppDispatch, useAppSelector } from '../../redux';
import { EditIcon } from '../../icons/EditIcon';
import { css } from '@emotion/react';
import { AvatarIcon } from '../../icons/AvatarIcon';
import ReviewForm from './ReviewForm';
import { useRouter } from 'next/router';


const styles = {
  actions: css({
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: '20px',
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
  }),
 
};

const ReviewCardItem: React.FC<{ review: Review }> = ({ review }) => {
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleEditReview = useCallback(() => {
    dispatch(moviesActions.editingMovieReview(review));
  }, [dispatch, review]);

  const handleDeleteReview = useCallback(() => {
    dispatch(moviesActions.deleteMovieReviewEpic({ id: review.id }));
  }, [dispatch, review.id]);

  const { user } = useAppSelector((state) => state.users);
  const shouldShowActions = user && user.id === review.reviewer?.id;
  const editingReview = useAppSelector((state) => state.movies.editingReview);

  const { movie } = useAppSelector(
    (state) => state.movies.reviews
  );

  const handleClick = useCallback(
    (idUser: string) => router.push(`/reviews/users/${idUser}`),
    [router]
  );


  return (
    <Card sx={{
      backgroundColor: '#121212',
      color: '#fff',
      boxShadow: '0px 4px 50px rgba(0, 0, 0, 0.59)',
    }}>
      {editingReview?.id === review.id ?
        <ReviewForm movieId={movie?.id as string} />
        :
        <>
          <CardHeader
            title={review?.movieByMovieId?.title || '' + review.title}
            titleTypographyProps={{ variant: 'h6' }}
            subheader={'@' + review.reviewer?.name}
            avatar={<AvatarIcon />}
            subheaderTypographyProps={{
              variant: 'subtitle2',
              color: '#FF6B00',
              sx: {cursor: 'pointer'},
              onClick: () => handleClick(review.reviewer?.id as string),
            }}
          />
          <CardContent sx={{ paddingTop: 0 }}>
            <Stack direction="column" spacing={2}>
              <div>{review.body}</div>
              <div css={styles.actions}>
                <Rating value={review.rating || 0} readOnly />
                {shouldShowActions && (
                  <Stack direction="row" sx={{marginTop: {xs: '1rem'}}} spacing={2}>
                    <Button
                      endIcon={<EditIcon color={'primary'} />}
                      variant={'outlined'}
                      onClick={handleEditReview}
                    >
                      Edit
                    </Button>
                    <Button
                      endIcon={<Delete />}
                      color={'error'}
                      variant={'outlined'}
                      onClick={handleDeleteReview}
                    >
                      Delete
                    </Button>
                  </Stack>
                )}
              </div>
            </Stack>
          </CardContent>
        </>


      }
    </Card >
  );
};

export default ReviewCardItem;
