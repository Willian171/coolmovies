import React, { FC, useCallback, useEffect, useState } from 'react';
import {
  Button,
  Card,
  CardContent,
  Grid,
  Rating,
  TextField,
} from '@mui/material';
import { moviesActions, useAppDispatch, useAppSelector } from '../../redux';
import { actions } from '../../redux/slices/movies/moviesSlice';
import { css } from '@emotion/react';


const styles = {
  card: css({
    paddingBottom: '20px',
  }),
  actions: css({
    display: 'flex',
    columnGap: '15px',
    justifyContent: 'end',
  }),
  input: css({
    border: '1px solid #454545',
    borderRadius: '10px',
    backgroundColor: '#121212',
    transition: 'all 0.3s ease-in-out',
  }),
  icon: css({
    '& .MuiRating-icon': {
      color: "#faaf00"
    }
  }),
};

const ReviewForm: FC<{movieId: string}> = ({ movieId }) => {
  const [title, setTitle] = useState<string | undefined>('');
  const [body, setBody] = useState<string | undefined>('');
  const [rating, setRating] = useState<number | undefined>(0);
  const [errors, setErrors] = useState<any | undefined>();
  const handleChange = useCallback(
    (fieldName, updateField) => (e: any) => updateField(e.currentTarget?.value),
    []
  );
  const handleRating = useCallback(
    (updateField) => (event: React.SyntheticEvent<Element, Event>, newValue: number | null) => updateField(newValue),
    []
  );
  const dispatch = useAppDispatch();
  const handleCancelReview = useCallback(() => {
    dispatch(moviesActions.cancelMovieReview());
  }, [dispatch]);
  const { user } = useAppSelector((state) => state.users);

  const reviewToUpdate = useAppSelector((state) => state.movies?.editingReview);
  useEffect(() => {
    setTitle(reviewToUpdate?.title);
    setBody(reviewToUpdate?.body);
    setRating(reviewToUpdate?.rating);
  }, [reviewToUpdate]);

  const onSubmit = useCallback(
    (title, body, rating) => {
      return () => {
        let errors;

        if (!title) {
          errors = { ...(errors || {}), title: 'Required' };
        }
        if (!body) {
          errors = { ...(errors || {}), body: 'Required' };
        }
        setErrors(errors);

        if (errors) {
          return;
        }

        if (reviewToUpdate?.id) {
          const { id } = reviewToUpdate;
          dispatch(
            actions.updateMovieReviewEpic({
              id,
              movieReviewPatch: {
                title,
                body,
                rating: rating || 0,
                userReviewerId: user?.id,
                movieId,
              },
            })
          );
          return;
        }
        dispatch(
          actions.createMovieReviewEpic({
            title,
            body,
            rating: rating || 0,
            userReviewerId: user?.id,
            movieId,
          })
        );
      };
    },
    [dispatch, user?.id, movieId, reviewToUpdate]
  );

  return (
    <Card css={styles.card}>
      <CardContent>
        <Grid container spacing={2} >
          <Grid item xs={12}>
            <TextField
              id="title"
              name="title"
              label="Title"
              css={styles.input}
              fullWidth
              onChange={handleChange('title', setTitle)}
              helperText={errors?.title || ''}
              error={!!errors?.title}
              value={title}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="body"
              name="body"
              label="Review"
              css={styles.input}
              multiline
              fullWidth
              minRows={7}
              onChange={handleChange('body', setBody)}
              helperText={errors?.body || ''}
              error={!!errors?.body}
              value={body}
              sx={{ borderRadius: '10px'}}
            />
          </Grid>
          <Grid item xs={12}>
            <Rating
              id="rating"
              name="rating"
              css={styles.icon}
              value={rating}
              onChange={handleRating(setRating)}
            />
          </Grid>
          <Grid item xs={12}>
            <div css={styles.actions}>
              <Button
                variant={'outlined'}
                color={'error'}
                onClick={handleCancelReview}
              >
                Cancel
              </Button>
              <Button
                variant={'outlined'}
                color={'secondary'}
                onClick={onSubmit(title, body, rating)}
              >
                Save
              </Button>
            </div>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default ReviewForm;
