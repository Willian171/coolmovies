import React, { useEffect } from 'react';
import { Review } from '../../interfaces/Review';
import ReviewCardItem from './ReviewCardItem';
import { css } from '@emotion/react';
import ScrollReveal from '../../components/ScrollReveal';

const styles = {
  root: css({
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    rowGap: '50px',
  }),
  addReview: css({
    width: '100%',
    textAlign: 'end',
  }),
};

const ReviewsList: React.FC<{ reviews?: Review[] }> = ({ reviews }) => {
  return (
    <div css={styles.root}>
      {reviews?.map((review) => (
        <ScrollReveal key={review.id}>
          <ReviewCardItem key={review.id} review={review} />
        </ScrollReveal>
      ))}
    </div>
  );
};

export default ReviewsList;
