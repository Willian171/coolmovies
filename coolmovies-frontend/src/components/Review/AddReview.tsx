import { Button } from '@mui/material';
import React, { FC, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { moviesActions, useAppSelector } from '../../redux';
import { css } from '@emotion/react';
import { Add } from '@mui/icons-material';


const styles = {
  button: css({
    position: 'absolute',
    right: '6rem',
    top: '11rem',
    transition: 'all 0.3s ease-in-out'
  }),
};
const AddReview: FC = ({ children }) => {
  const creatingReview = useAppSelector((state) => state.movies.creatingReview);
  const { user } = useAppSelector((state) => state.users);
  const dispatch = useDispatch();
  const handleAddReview = useCallback(() => {
    dispatch(
      moviesActions.creatingMovieReview({
        id: '',
        title: '',
        body: '',
        rating: 0,
      })
    );
  }, [dispatch]);
  return (
    <>
      {!!creatingReview || user && (
        <Button
          css={styles.button}
          color={'secondary'}
          endIcon={<Add />}
          variant={'outlined'}
          onClick={handleAddReview}>
          Create
        </Button>
      )}
    </>
  );
};

export default AddReview;
