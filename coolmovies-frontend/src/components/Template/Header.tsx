import React, { useCallback } from 'react';
import {
  AppBar,
  Box,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@mui/material';
import { User } from '../../interfaces/User';
import { useAppDispatch, useAppSelector, usersActions } from '../../redux';
import { useEffect } from 'react';
import { AvatarIcon } from '../../icons/AvatarIcon';
import { css } from '@emotion/react';
import Link from 'next/link';


const styles = {
  logo: css({
    color: '#FF6B00',
    fontSize: '1.6rem',
    fontWeight: 'bold',
  }),
  link: css({
    color: '#FF6B00',
  }),
}

const Header = () => {

  const dispatch = useAppDispatch();
  const { user, data } = useAppSelector((state) => state.users);
  useEffect(() => {
    dispatch(usersActions.fetch());
  }, [dispatch]);

  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );

  const handleOpenUserMenu = useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      setAnchorElUser(event.currentTarget);
    },
    []
  );

  const handleCloseUserMenu = useCallback(() => {
    setAnchorElUser(null);
  }, []);

  const handleUpdateUser = useCallback(
    (user: User) => () => {
      dispatch(usersActions.updateUser(user));
      handleCloseUserMenu();
    },
    [handleCloseUserMenu, user]
  );


  return (
    <AppBar position={'static'} color={'transparent'} sx={{
      boxShadow: 'none',
      flexDirection: { xs: 'column', md: 'row' },
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '1rem 2rem',
      marginBottom: '1.5em',
      flexWrap: 'wrap',
    }}>
      <Typography css={styles.logo} variant="h2" component="h2">Coolmovies</Typography>
      <Box sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: { xs: '100%', sm: '80%', md: '60%', lg: '40%' },
        flexWrap: 'wrap',
        flexDirection: { xs: 'column', md: 'row' },
      }}>
        <Link css={styles.link} href="/">Moovies</Link>
        <Link css={styles.link} href={`/reviews/users/${user && user.id}`}>My Reviews</Link>
        <Toolbar>
          <Box sx={{ flexGrow: 1 }} onClick={handleOpenUserMenu}>
            <IconButton color="inherit" >
              <AvatarIcon />
            </IconButton>
            <Typography sx={{ cursor: 'pointer', fontSize: '1rem', display: 'inline-block' }} variant="h6" component="h6">
              {user?.name}
            </Typography>

          </Box>
          <Menu
            id="menu-appbar"
            anchorEl={anchorElUser}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={Boolean(anchorElUser)}
            onClose={handleCloseUserMenu}
          >
            {data?.map((user) => (
              <MenuItem key={user.id} onClick={handleUpdateUser(user)} >
                {user.name}
              </MenuItem>
            ))}
          </Menu>
        </Toolbar>
      </Box>
    </AppBar>
  );
};

export default Header;