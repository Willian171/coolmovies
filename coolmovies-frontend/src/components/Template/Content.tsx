import React from 'react';
import { styled } from '@mui/system';

const Content = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  flexGrow: 1,
});

export default Content
