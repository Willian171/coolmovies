import React, { FC } from 'react';
import Container from './Container';
import Content from './Content';
import Header from './Header';

const Template: FC = ({ children }) => {
  return (
    <Container>
      <Header />
      <Content>{children}</Content>
    </Container>
  );
};

export default Template;
