import React from 'react';
import { styled } from '@mui/system';

const Container = styled('div')({
  backgroundColor: '#0D0D0D',
  display: 'flex',
  flexDirection: 'column',
  minHeight: '100vh',
  color: '#fff',
  width: '100%',
});

export default Container