import React, { useRef, useEffect, FC, useState } from "react";

const ScrollReveal: FC = ({ children }) => {
  const sectionRef = useRef<HTMLElement>(null);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    async function animate() {
      if (sectionRef.current) {
        const scrollreveal = (await import("scrollreveal")).default
        scrollreveal().reveal(sectionRef.current, {delay: 500});
      }
    }
    animate()
    setTimeout(() => {
      setLoading(false)
    }, 0)
  }, []);

  return (
    <section
      ref={sectionRef}
      className="container scroll-section"
      data-testid="section"
    >
      {!loading && children}
    </section>
  );
};

export default ScrollReveal;