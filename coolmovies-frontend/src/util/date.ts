import moment from "moment";

export const formatDate = (date: Date | string, format: string = "DD/MM/YYYY") => {
    const dateFormatted = moment(date).format(format);
    return dateFormatted;
};
