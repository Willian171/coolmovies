import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Review } from '../../../interfaces/Review';
import { User } from '../../../interfaces/User';

interface UsersState {
  data?: User[];
  user?: User;
  loading: boolean;
  error?: string;
  reviews?: Review[]
}

const initialState: UsersState = {
  loading: false,
};

export const usersSlice = createSlice({
  initialState,
  name: 'users',
  reducers: {
    fetch: (state) => {
      state.loading = true;
      state.error = undefined;
    },
    fetchReviewsByUserId: (
      state, 
      action: PayloadAction<{ userId: string }>
      ) => {
      state.loading = true;
    },
    updateUser: (state, { payload }: PayloadAction<User>) => {
      state.user = payload;
      state.error = undefined;
    },
    loaded: (state, action: PayloadAction<{ data: User[] }>) => {
      state.loading = false;
      state.data = action.payload.data;
      state.user = action.payload.data[0];
      state.error = undefined;
    },
    loadedReviewsByUserId: (state, action: PayloadAction<{ data: Review[] }>) => {
      state.loading = false;
      state.reviews = action.payload.data;
      state.error = undefined;
    },

    loadError: (state) => {
      state.loading = false;
      state.error = 'Error fetching users';
    },
  },
});

export const { actions } = usersSlice;
export type SliceAction = typeof actions;
export default usersSlice.reducer;
