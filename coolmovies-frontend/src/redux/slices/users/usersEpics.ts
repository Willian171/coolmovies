import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../types';
import { actions, SliceAction } from './usersSlice';
import { allUsersQuery } from '../../../services/queries/allUsersQuery';
import { allReviewsByUserIdQuery } from '../../../services/queries/allReviewsByUserIdQuery';

export const usersAsyncEpic: Epic = (
  action$: Observable<SliceAction['fetch']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async () => {
      try {
        const result = await client.query({
          query: allUsersQuery,
        });
        return actions.loaded({ data: result.data.allUsers.nodes });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const reviewsByUserAsyncEpic: Epic = (
  action$: Observable<SliceAction['fetchReviewsByUserId']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetchReviewsByUserId.match),
    switchMap(async ({ payload }) => {
      try {
        const result = await client.query({
          query: allReviewsByUserIdQuery,
          variables: {
            userId: payload.userId,
          },
        });
        return actions.loadedReviewsByUserId({ data: result.data.reviews.nodes });
      } catch (err) {
        return actions.loadError();
      }
    })
  );
