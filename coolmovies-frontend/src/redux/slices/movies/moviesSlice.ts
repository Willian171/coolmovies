import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Movie } from '../../../interfaces/Movie';
import { ReviewInput } from '../../../interfaces/ReviewInput';
import { Review } from '../../../interfaces/Review';

interface ReviewsAction {
  movie: Movie;
  reviews: { nodes: Review[] };
}

interface ReviewsState {
  movie?: Movie;
  reviews?: Review[];
  loading: boolean;
}

interface MoviesState {
  data?: Movie[];
  loading: boolean;
  error?: string;
  editingReview?: Review;
  creatingReview?: Review;
  reviews: ReviewsState;
}

const initialState: MoviesState = {
  loading: false,
  reviews: {
    loading: false,
  },
};

export const moviesSlice = createSlice({
  initialState,
  name: 'movies',
  reducers: {
    fetch: (state) => {
      state.loading = true;
      state.error = undefined;
    },
    fetchReviewsByMovieId: (
      state,
      action: PayloadAction<{ movieId: string }>
    ) => {
      state.reviews.loading = true;
    },
    reviewsLoaded: (state, { payload }: PayloadAction<ReviewsAction>) => {
      state.reviews.loading = false;
      state.reviews.movie = payload.movie;
      state.reviews.reviews = payload.reviews.nodes;
    },
    clearData: (state) => {
      state.loading = false;
      state.data = undefined;
      state.error = undefined;
    },
    loaded: (state, action: PayloadAction<{ data: Movie[] }>) => {
      state.loading = false;
      state.data = action.payload.data;
      state.error = undefined;
    },
    loadError: (state) => {
      state.loading = false;
      state.error = 'Error Fetching :(';
    },
    editingMovieReview: (state, { payload }: PayloadAction<Review>) => {
      state.editingReview = payload;
      state.creatingReview = undefined;
    },
    creatingMovieReview: (state, { payload }: PayloadAction<Review>) => {
      state.creatingReview = payload;
      state.editingReview = undefined;
    },
    movieReviewAdded: (state, { payload }: PayloadAction<ReviewsAction>) => {
      state.editingReview = undefined;
      state.creatingReview = undefined;
      state.reviews.movie = payload.movie;
      state.reviews.reviews = payload.reviews.nodes;
    },
    cancelMovieReview: (state) => {
      state.editingReview = undefined;
      state.creatingReview = undefined;
    },
    createMovieReviewEpic: (state, action: PayloadAction<ReviewInput>) => {
      state.loading = true;
    },
    createReviewError: (state) => {
      state.error = 'Error saving review';
    },
    updateMovieReviewEpic: (
      state,
      action: PayloadAction<{ id: string; movieReviewPatch: ReviewInput }>
    ) => {
      state.loading = true;
    },
    deleteMovieReviewEpic: (state, action: PayloadAction<ReviewInput>) => {},
    deleteMovieReviewCompletedEpic: (
      state,
      { payload }: PayloadAction<ReviewsAction>
    ) => {
      state.editingReview = undefined;
      state.creatingReview = undefined;
      state.reviews.movie = payload.movie;
      state.reviews.reviews = payload.reviews.nodes;
    },
  },
});

export const { actions } = moviesSlice;
export type SliceAction = typeof actions;
export default moviesSlice.reducer;
