export { actions as moviesActions } from './moviesSlice';
export { default as moviesReducer } from './moviesSlice';
import { combineEpics } from 'redux-observable';
import {
  createMovieReviewAsyncEpic,
  deleteMovieReviewAsyncEpic,
  moviesAsyncEpic,
  reviewsAsyncEpic,
  updateMovieReviewAsyncEpic,
} from './moviesEpics';

export const moviesEpics = combineEpics(
  moviesAsyncEpic,
  createMovieReviewAsyncEpic,
  updateMovieReviewAsyncEpic,
  deleteMovieReviewAsyncEpic,
  reviewsAsyncEpic
);
