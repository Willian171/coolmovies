import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../types';
import { actions, SliceAction } from './moviesSlice';
import { allMoviesQuery } from '../../../services/queries/allMoviesQuery';
import { createMovieReviewMutation } from '../../../services/mutations/createMovieReviewMutation';
import { allReviewsByMovieIdQuery } from '../../../services/queries/allReviewsByMovieIdQuery';
import { deleteMovieReviewMutation } from '../../../services/mutations/deleteMovieReviewMutation';
import { updateMovieReviewMutation } from '../../../services/mutations/updateMovieReviewMutation';

export const moviesAsyncEpic: Epic = (
  action$: Observable<SliceAction['fetch']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async () => {
      try {
        const result = await client.query({
          query: allMoviesQuery,
        });
        return actions.loaded({ data: result.data.allMovies.nodes });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const reviewsAsyncEpic: Epic = (
  action$: Observable<SliceAction['fetchReviewsByMovieId']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetchReviewsByMovieId.match),
    switchMap(async ({ payload }) => {
      try {
        const result = await client.query({
          query: allReviewsByMovieIdQuery,
          variables: {
            movieId: payload.movieId,
          },
        });
        return actions.reviewsLoaded(result.data);
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const createMovieReviewAsyncEpic: Epic = (
  action$: Observable<SliceAction['createMovieReviewEpic']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.createMovieReviewEpic.match),
    switchMap(async ({ payload }) => {
      try {
        const result = await client.mutate({
          mutation: createMovieReviewMutation,
          variables: {
            movieReviewInput: { movieReview: payload },
          },
        });
        const movie = result.data.createMovieReview.movie;
        const reviews = result.data.createMovieReview.movie.reviews;
        return actions.movieReviewAdded({ movie, reviews });
      } catch (err) {
        return actions.createReviewError();
      }
    })
  );

export const updateMovieReviewAsyncEpic: Epic = (
  action$: Observable<SliceAction['updateMovieReviewEpic']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.updateMovieReviewEpic.match),
    switchMap(async ({ payload }) => {
      try {
        const result = await client.mutate({
          mutation: updateMovieReviewMutation,
          variables: {
            movieReviewInput: payload,
          },
        });
        const movie = result.data.updateMovieReviewById.movie;
        const reviews = result.data.updateMovieReviewById.movie.reviews;
        return actions.movieReviewAdded({ movie, reviews });
      } catch (err) {
        return actions.createReviewError();
      }
    })
  );

export const deleteMovieReviewAsyncEpic: Epic = (
  action$: Observable<SliceAction['deleteMovieReviewEpic']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.deleteMovieReviewEpic.match),
    switchMap(async ({ payload }) => {
      try {
        const result = await client.mutate({
          mutation: deleteMovieReviewMutation,
          variables: {
            id: payload,
          },
        });
        const movie = result.data.deleteMovieReviewById.movie;
        const reviews = result.data.deleteMovieReviewById.movie.reviews;
        return actions.deleteMovieReviewCompletedEpic({ movie, reviews });
      } catch (err) {
        return actions.createReviewError();
      }
    })
  );
