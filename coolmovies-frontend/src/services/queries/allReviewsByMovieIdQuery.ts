import { gql } from '@apollo/client';

export const allReviewsByMovieIdQuery = gql`
  query MovieById($movieId: UUID!) {
    movie: movieById(id: $movieId) {
      title
      releaseDate
      imgUrl
      director: movieDirectorByMovieDirectorId {
        name
      }
    }
    reviews: allMovieReviews(condition: { movieId: $movieId }) {
      nodes {
        id
        title
        body
        rating
        reviewer: userByUserReviewerId {
          id
          name
        }
      }
    }
  }
`;
