import { gql } from '@apollo/client';

export const allUsersQuery = gql`
  query AllUsers {
    allUsers {
      nodes {
        id
        name
      }
    }
  }
`;
