import { gql } from '@apollo/client';

export const allReviewsByUserIdQuery = gql`
  query UserById($userId: UUID!) {
    reviews: allMovieReviews(condition: { userReviewerId: $userId }) {
      nodes {
        id
        title
        body
        rating
        reviewer: userByUserReviewerId {
          id
          name
        }
        movieByMovieId {
          title
        }
      }
    }
  }
`;
