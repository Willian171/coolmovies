import { gql } from '@apollo/client';

export const createMovieReviewMutation = gql`
  mutation Mutation($movieReviewInput: CreateMovieReviewInput!) {
    createMovieReview(input: $movieReviewInput) {
      movie: movieByMovieId {
        title
        releaseDate
        imgUrl
        director: movieDirectorByMovieDirectorId {
          name
        }
        reviews: movieReviewsByMovieId {
          nodes {
            id
            title
            body
            rating
            reviewer: userByUserReviewerId {
              id
              name
            }
          }
        }
      }
    }
  }
`;
