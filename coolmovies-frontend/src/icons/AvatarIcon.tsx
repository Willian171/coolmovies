import React, { FC } from 'react';
import { SvgIcon } from '@mui/material';
import Image from 'next/image';

export const AvatarIcon = (props: any) => {
  return (
    <SvgIcon
      {...props}
      component={() => <Image src={'/avatar.svg'} width={60} height={60} />}
      inheritViewBox
    />
  );
};
