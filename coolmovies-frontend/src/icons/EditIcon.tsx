import React, { FC } from 'react';
import { SvgIcon } from '@mui/material';
import Image from 'next/image';

export const EditIcon = (props: any) => {
  return (
    <SvgIcon
      {...props}
      component={() => <Image src={'/edit.svg'} width={20} height={20} />}
      inheritViewBox
    />
  );
};
