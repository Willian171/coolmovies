import { Movie } from "./Movie";

export interface Review {
  id: string;
  title?: string;
  body?: string;
  rating?: number;
  reviewer?: Reviewer;
  movieByMovieId?: Movie;
}

interface Reviewer {
  id: string;
  name: string;
}
