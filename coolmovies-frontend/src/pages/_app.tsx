import '../styles/globals.css';
import type { AppProps } from 'next/app';
import React, { FC, useMemo, useState } from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import Head from 'next/head';
import { createStore } from '../redux';
import { EnhancedStore } from '@reduxjs/toolkit';
import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
} from '@apollo/client';
import { CssBaseline } from '@mui/material';
import Template from '../components/Template';
import { ThemeProvider } from '@mui/material/styles';
import themeMoovies from '../styles/themeMoovies';

const App: FC<AppProps> = ({ Component, pageProps }) => {
  const [store, setStore] = useState<EnhancedStore | null>(null);
  const apolloClient = useMemo(() => {
    const client = new ApolloClient({
      cache: new InMemoryCache(),
      uri: '/graphql',
    });

    const store = createStore({ epicDependencies: { client } });
    setStore(store);

    return client;
  }, []);
  if (!store) return <>{'Loading...'}</>;
  return (
    <>
      <Head>
        <title>{'Coolmovies Frontend'}</title>
        <meta charSet='UTF-8' />
        <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
      </Head>
      <ThemeProvider theme={themeMoovies}>
        <ReduxProvider store={store}>
          <ApolloProvider client={apolloClient}>
            <CssBaseline />
            <Template>
              <Component {...pageProps} />
            </Template>
          </ApolloProvider>
        </ReduxProvider>
      </ThemeProvider>
    </>
  );
};

export default App;
