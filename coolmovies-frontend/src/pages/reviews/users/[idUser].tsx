import { css } from "@emotion/react";
import { useRouter } from "next/router";
import { FC, useEffect } from "react";
import ReviewsList from "../../../components/Review/ReviewsList";
import { useAppDispatch, useAppSelector, usersActions } from "../../../redux";

const styles = {
    root: css({
      margin: '32px 6.5vw',
    }),
  };

const ReviewsByUser: FC = () => {
    const dispatch = useAppDispatch();
    const router = useRouter();
    const { idUser } = router.query;

    const { reviews } = useAppSelector(
        (state) => state.users
    );

    useEffect(() => {
        dispatch(usersActions.fetchReviewsByUserId({ userId: idUser as string }));
    }, [dispatch, idUser]);

    return (
        <div css={styles.root}>
            <ReviewsList reviews={reviews} />
        </div>
    )
}

export default ReviewsByUser;