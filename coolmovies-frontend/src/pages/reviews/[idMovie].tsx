import React, { FC, useEffect } from 'react';
import { useRouter } from 'next/router';
import MovieTitle from '../../components/Movie/MovieTitle';
import MovieImage from '../../components/Movie/MovieImage';
import ReviewsList from '../../components/Review/ReviewsList';
import ReviewForm from '../../components/Review/ReviewForm';
import { moviesActions, useAppDispatch, useAppSelector } from '../../redux';
import { css } from '@emotion/react';
import AddReview from '../../components/Review/AddReview';
import { Review } from '../../interfaces/Review';

const styles = {
  root: css({
    maxWidth: '100%',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    margin: '32px 6.5vw',
  }),
  movieSection: css({
    width: '100%',
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
    columnGap: 80,
  }),
  image: css({
    borderRadius: '5px',
  }),
  reviewSection: css({
    flexGrow: 2,
    flexBasis: '60%',
    width: '100%',
  }),
};

const Reviews: FC = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { idMovie } = router.query;

  const { movie, reviews } = useAppSelector(
    (state) => state.movies.reviews
  );

  useEffect(() => {
    dispatch(moviesActions.fetchReviewsByMovieId({ movieId: idMovie as string }));
  }, [dispatch, idMovie]);

  const creatingReview = useAppSelector((state) => state.movies.creatingReview);

  return (
    <div css={styles.root}>
      <AddReview />
      <MovieTitle movie={movie} reviews={reviews as Review[]} />
      <div css={styles.movieSection}>
        <MovieImage imgUrl={movie?.imgUrl} />
        <div css={styles.reviewSection}>
          {creatingReview && <ReviewForm movieId={idMovie as string} />}
          <ReviewsList reviews={reviews} />
        </div>
      </div>
    </div>
  );
};

export default Reviews;
