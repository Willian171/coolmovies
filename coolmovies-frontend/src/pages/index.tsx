import type { NextPage } from 'next';
import MovieList from '../components/Movie/MovieList';


const Home: NextPage = () => {
  return (
    <MovieList />
  );
};

export default Home;
